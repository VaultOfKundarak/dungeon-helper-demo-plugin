﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VoK.Sdk;
using VoK.Sdk.Ddo;
using VoK.Sdk.Plugins;

namespace VoK.DemoPlugin
{
    public class DemoPlugin : IDdoPlugin
    {
        //TODO: Generate new guid (guidgenerator.com works well)
        private static Guid _pluginId = Guid.Parse("dfbd57bc-a5fb-4cb5-b02d-547bfeb74698");
        private DemoPluginUI _ui;
        private IGameDataProvider _dataProvider;
        private string _folder;

        public Guid PluginId => _pluginId;

        public GameId Game => GameId.DDO;

        //TODO: Generate new guid (guidgenerator.com works well)
        public string PluginKey => "24f66368-12cb-4600-aee2-020a053422ca";

        // TODO: customize this
        public string Name => "Demo Plugin";

        // TODO: customize this
        public string Description => "Sample plugin to show people how to make them";

        // TODO: customize this
        public string Author => "Morrikan";

        public Version Version => this.GetType().Assembly.GetName().Version;

        public IPluginUI GetPluginUI()
        {
            return _ui;
        }

        public void Initialize(IDdoGameDataProvider gameDataProvider, string folder)
        {
            _dataProvider = gameDataProvider;
            _ui = new DemoPluginUI(gameDataProvider);
            _folder = folder;
        }

        public void Terminate()
        {
            _ui.Terminate();
        }
    }
}
