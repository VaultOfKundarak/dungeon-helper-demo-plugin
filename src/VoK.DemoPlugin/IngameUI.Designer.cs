﻿
namespace VoK.DemoPlugin
{
    partial class IngameUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lInfo = new System.Windows.Forms.Label();
            this.lHeader = new System.Windows.Forms.Label();
            this.btnExample = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lInfo
            // 
            this.lInfo.AutoSize = true;
            this.lInfo.BackColor = System.Drawing.Color.Transparent;
            this.lInfo.Font = new System.Drawing.Font("Neo Sans", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lInfo.ForeColor = System.Drawing.Color.LightGray;
            this.lInfo.Location = new System.Drawing.Point(13, 46);
            this.lInfo.Name = "lInfo";
            this.lInfo.Size = new System.Drawing.Size(317, 15);
            this.lInfo.TabIndex = 0;
            this.lInfo.Text = "A demo for DDO\'s only plugin framework, Dungeon Helper";
            // 
            // lHeader
            // 
            this.lHeader.AutoSize = true;
            this.lHeader.BackColor = System.Drawing.Color.Transparent;
            this.lHeader.Font = new System.Drawing.Font("Neo Sans", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lHeader.ForeColor = System.Drawing.Color.LightGray;
            this.lHeader.Location = new System.Drawing.Point(12, 9);
            this.lHeader.Name = "lHeader";
            this.lHeader.Size = new System.Drawing.Size(166, 33);
            this.lHeader.TabIndex = 1;
            this.lHeader.Text = "Demo Plugin";
            // 
            // btnExample
            // 
            this.btnExample.Location = new System.Drawing.Point(106, 126);
            this.btnExample.Name = "btnExample";
            this.btnExample.Size = new System.Drawing.Size(75, 23);
            this.btnExample.TabIndex = 2;
            this.btnExample.Text = "Example";
            this.btnExample.UseVisualStyleBackColor = true;
            this.btnExample.Click += new System.EventHandler(this.btnExample_Click);
            // 
            // IngameUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.BackgroundImage = global::VoK.DemoPlugin.Properties.Resources.body_fill;
            this.ClientSize = new System.Drawing.Size(477, 220);
            this.ControlBox = false;
            this.Controls.Add(this.btnExample);
            this.Controls.Add(this.lHeader);
            this.Controls.Add(this.lInfo);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Neo Sans", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(477, 220);
            this.Name = "IngameUI";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "IngameUI";
            this.Load += new System.EventHandler(this.IngameUI_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lInfo;
        private System.Windows.Forms.Label lHeader;
        private System.Windows.Forms.Button btnExample;
    }
}