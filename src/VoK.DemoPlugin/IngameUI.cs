﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using VoK.Sdk.Ddo;
using VoK.Sdk.Enums;
using VoK.Sdk.Helpers;
using VoK.Sdk.Properties;

namespace VoK.DemoPlugin
{
    public partial class IngameUI : Form
    {
        private IDdoGameDataProvider _provider;

        public IngameUI(IDdoGameDataProvider provider)
        {
            _provider = provider;
            InitializeComponent();
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        private void IngameUI_Load(object sender, EventArgs e)
        {
        }

        private void btnExample_Click(object sender, EventArgs e)
        {
            // just an example of how to use the SDK
            var myId = _provider.GetCurrentCharacterId();
        }
    }
}
