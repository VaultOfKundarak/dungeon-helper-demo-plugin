﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using VoK.Sdk.Ddo;
using VoK.Sdk.Plugins;

namespace VoK.DemoPlugin
{
    public class DemoPluginUI : IPluginUI
    {
        private IDdoGameDataProvider _provider;
        private IngameUI _ingameUi;

        public DemoPluginUI(IDdoGameDataProvider provider)
        {
            _provider = provider;
            _ingameUi = new IngameUI(provider);
        }

        public float? FocusedOpacity => 1.0f;

        public bool EnabledInCharacterSelection => false;

        public Image ToolbarImage
        {
            get
            {
                //TODO: specify your ingame icon
                Stream iconStream = Assembly.GetAssembly(typeof(DemoPluginUI)).GetManifestResourceStream("VoK.DemoPlugin.images.icon.png");
                return Image.FromStream(iconStream);
            }
        }

        public object UserInterfaceForm => _ingameUi;

        //TODO: Define minimum UI size
        public Tuple<int, int> MinSize => new(400, 200);

        public void Terminate()
        {
            // clean up anything you need to clean up when the plugin unloads
        }
    }
}
