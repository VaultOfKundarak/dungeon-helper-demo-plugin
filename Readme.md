# Demo Plugin #

This is a sample / starter plugin for the Dungeon Helper Framework

## Requirements ##

- Windows 10 or 11, current releases (though we use 10)
- Dotnet 6 SDKs - You will need the x64 .NET 6.0 SDK. Link: [https://dotnet.microsoft.com/en-us/download/visual-studio-sdks](https://dotnet.microsoft.com/en-us/download/visual-studio-sdks)
- Visual Studio 2022 - Link: [https://visualstudio.microsoft.com/vs/community/](https://visualstudio.microsoft.com/vs/community/)
